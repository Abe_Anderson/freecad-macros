#The following code is from the the document 
#	Getting started with FreeCAD scripting
#	by J. Rabault 23rd January 2017
#
#https://folk.uio.no/jeanra/Teaching/MEK3230/Documents/GettingStartedFreeCADScripting.pdf
#
#https://folk.uio.no/jeanra/author.html
#
#The website states:
#Content released under MIT License 
#(c) 2015 Jean Rabault, contact: jeanra@math.uio.no
#
#https://folk.uio.no/jeanra/License.html
#
#The code is to import freecad python modules in to python console interfaces other than freecad itself.
#
#Notes by Abe Anderson 2-23-2019 
#https://wiki.opensourceecology.org/wiki/AbeAnd_Log



import numpy as np
import sys

# path to FreeCAD; I found it from:
# $ locate FreeCAD.so
FREECADPATH = '/usr/lib/freecad-0.16/lib/'


def import_fcstd(path_freecad):
    """try to import FreeCAD on path_freecad"""
    sys.path.append(path_freecad)
    try:
        import FreeCAD
    except:
        print "Could not import FreeCAD"
        print "Are you using the right path?"

import_fcstd(FREECADPATH)

